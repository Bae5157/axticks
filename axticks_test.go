// 22 Dec 2021

package axticks

import (
	"testing"
)

type f64 []float64
type ad AxisDscrpt

var test1set = []struct {
	x  []float64
	ad ad // really an  AxisDscrpt
}{
	{f64{18, 19}, ad{18, 19, .1, 1}},
	{f64{-0.01, 0.05}, ad{-0.01, 0.05, 0.01, 2}},
	{f64{1, 2}, ad{1, 2, 0.1, 1}},
	{f64{1, 100}, ad{0, 100, 10, 0}},
	{f64{-100, -99}, ad{-100, -99, 0.1, 1}},
	{f64{-0.1, -0.05}, ad{-0.1, -0.05, .005, 3}},
	{f64{-2, 200, -2}, ad{-50, 200, 50, 0}},
	{f64{-2, 2}, ad{-2, 2, 0.5, 1}},
	{f64{-2, 2, -2}, ad{-2, 2, 0.5, 1}},
	{f64{-0.01, 1}, ad{-0.2, 1, 0.2, 1}},
	{f64{-0.01, 300}, ad{-50, 300, 50, 0}},
}

func TestDsc(t *testing.T) {
	const flds = "xmin, xmax, delta, precision."
	for _, tt := range test1set {
		if adDsc, err := Tickpos(tt.x); err != nil {
			t.Fatal("Got error", err, "on", tt.x)
		} else {
			if tt.ad != ad(adDsc) {
				t.Fatal(flds, "from", tt.x, "Wanted", tt.ad, "got", adDsc)
			}
		}
	}
}

// These cases should report an error
var testErrSet = []struct {
	x  []float64
	ad ad // really an  AxisDscrpt
}{
	{f64{1}, ad{1, 2, 0.2, 1}},           // only one value
	{f64{-2, -2, -2}, ad{-2, 2, 0.5, 1}}, // xmin and max the same
}

func TestErr(t *testing.T) {
	for _, tt := range testErrSet {
		if _, err := Tickpos(tt.x); err == nil {
			t.Fatal("No error reported from", tt.x)
		}
	}
}

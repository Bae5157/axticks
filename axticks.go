// 21 Dec 2021
// Heckberts method of generating nice numbers for a range and tickmarks.

package axticks

import (
	"errors"
	"fmt"
	"math"
)

const (
	RndDown = iota
	RndUp
)

// pow10 returns 10 to the x'th power. Returns an int, unlike the
// standard library version.
func pow10(x int) int64 {
	if x == 0 {
		return 1
	}
	eAbs := x
	if eAbs < 0 {
		eAbs = -eAbs
	}
	var r int64 = 1
	for f := 0; f < eAbs; f = f + 1 {
		r = r * 10
	}
	return r
}

// minmax gives us the min and max of a slice
func minMax(x []float64) (float64, float64) {
	xmin, xmax := x[0], x[0]
	for _, t := range x {
		if t < xmin {
			xmin = t
		} else {
			if t > xmax {
				xmax = t
			}
		}
	}
	return xmin, xmax
}

// nicenum is 50% taken from the Heckbert paper. It returns a number
// divisible by 1, 2 or 5 when rounding down. This gives a nice delta,
// the spacing between ticks.
func nicenum(x float64, round byte) (float64, error) {
	if x == 0 {
		return 0, nil
	}
	e := int(math.Floor(math.Log10(x)))
	f := x / math.Pow10(e)
	if math.IsNaN(f) {
		return 0, errors.New("logarithm error")
	}
	var nf float64

	if round == RndDown {
		switch {
		case f < 1.5:
			nf = 1
		case f < 2:
			nf = 1.5
		case f < 3:
			nf = 2
		case f < 4:
			nf = 3
		case f < 5:
			nf = 4
		case f < 7:
			nf = 5
		default:
			nf = 5 // used to be floor
		}
	} else {
		switch {
		case f <= 1:
			nf = 1
		case f <= 2:
			nf = 2
		case f <= 5:
			nf = 5
		default:
			nf = 10
		}
	}
	switch {
	case x < 1:
		return nf / float64(pow10(e)), nil
	case x == 1:
		return nf, nil
	default: // 	case x >= 1:
		return nf * float64(pow10(e)), nil
	}

}

// An AxisDscrpt describes some reasonable default values for an axis.
type AxisDscrpt struct {
	Xmin, Xmax, Delta float64
	Prcsn             int // Digits precision for axis labels, ready for sprintf
}

// Tickpos takes a slice of numbers and returns an AxisDscrpt with possibly
// reasonable default values for an axis.
// We do not return an array of tick structures as this would be too specific
// for the plotting package I am playing with now and would mean we have
// to import that package to get the Tick definition.
// The caller should do something like
//	tickpos := make([]float64, ntick)
//	tickpos[0] = xmin
//	for i := 1; i < ntick; i++ {
//		tickpos[i] = tickpos[i-1] + dlta
//	}

func Tickpos(x []float64) (AxisDscrpt, error) {
	ntick := 8
	var err error
	var delta, rnge float64
	var axisDscrpt AxisDscrpt // What we will return
	if len(x) < 2 {
		return axisDscrpt, errors.New("too few data points")
	}
	xmin, xmax := minMax(x)
	realxmin := xmin
	if xmax-xmin < math.SmallestNonzeroFloat32 {
		return axisDscrpt, errors.New("max - min too small")
	}
	if rnge, err = nicenum(xmax-xmin, RndUp); err != nil {
		return axisDscrpt, fmt.Errorf("min-max range is zero or <0: %w", err)
	}

	if delta, err = nicenum(rnge/(float64(ntick)-1), RndDown); err != nil {
		return axisDscrpt, err
	}

	switch { // Minimum of range
	case xmin < 0:
		a := xmin / delta // how many deltas fit here ?
		xmin = math.Floor(a) * delta
	case xmin == 0:
		// do nothing
	case xmin < 1.05*delta:
		xmin = 0
	default:
		if xmin, err = nicenum(xmin, RndDown); err != nil {
			return axisDscrpt, err
		}
	}

	switch { // Max of range
	case xmax < -1.05*delta:
		a := xmax / delta
		xmax = math.Ceil(a) * delta
	case xmax <= 0:
		xmax = 0
	default: // a tight upper bound
	}

	// That was Heckbert style. Now, let us do the R thing and check if the lower
	// bound was too generous. Problem is, that I think when we are near zero,
	// we should leave the lower bound at zero.
	toomuch := realxmin -xmin
	howmany := toomuch / delta
	if howmany >= 2 { // This is too much whitespace
		xmin = xmin + math.Floor(howmany) * delta
	}
	
	rnge = xmax - xmin // Min & max have been rounded, so recalculate the range
	ntick = int(math.Round((rnge / delta) + 1))

	var prcsn int = 0
	if delta < 1 {
		prcsn = int(math.Floor(math.Log10(delta)))
		prcsn = -prcsn
	}

	return AxisDscrpt{xmin, xmax, delta, prcsn}, nil
}

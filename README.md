# axticks

Package `axticks` takes a series of values (currently float64) and calculates a minimum, maximum, tick distancs (delta) and precision for plotting an axis. The numbers should be reasonably rounded. The precision is suitable for putting straight into an sprintf() call. The values are returned in a `AxisDscrpt` structure.

The entry point is `Tickpos()`. It can return an error if given too few numbers or if the range of numbers is too small.

It was written to give me pretty axes when using [go-chart](https://github.com/wcharczuk/go-chart), but might be useful elsewhere.

It has the consequence that everything is done with `float64`, although this precision is absurd when we are talking about plotting. Single precision would be fine.
